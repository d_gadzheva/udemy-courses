'use strict';

const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const btnCloseModal = document.querySelector('.btn--close-modal');
const btnsOpenModal = document.querySelectorAll('.btn--show-modal');

const header = document.querySelector('.header');
const nav = document.querySelector('.nav');
const btnScrollTo = document.querySelector('.btn--scroll-to');
const section1 = document.querySelector('#section--1');

const tabs = document.querySelectorAll('.operations__tab');
const tabsContainer = document.querySelector('.operations__tab-container');
const tabsContent = document.querySelectorAll('.operations__content');

///////////////////////////////////////
// Modal window

// FUNCTION:
// Function to display modal
const openModal = function (e) {
  e.preventDefault();

  modal.classList.remove('hidden');
  overlay.classList.remove('hidden');
};

// FUNCTION:
// Function to hide modal
const closeModal = function () {
  modal.classList.add('hidden');
  overlay.classList.add('hidden');
};

// EVENT LISTENER:
// On open modal
btnsOpenModal.forEach(btn => btn.addEventListener('click', openModal));
// EVENT LISTENERS:
// On close modal
btnCloseModal.addEventListener('click', closeModal);
overlay.addEventListener('click', closeModal);
document.addEventListener('keydown', function (e) {
  // CASE: esc press
  if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
    closeModal();
  }
});

///////////////////////////////////////
// Page navigation

// Heavy on preformance
// document.querySelectorAll('.nav__link').forEach(function (el) {
//   el.addEventListener('click', function (e) {
//     e.preventDefault();
//     const id = this.getAttribute('href');
//     console.log(id);
//     document.querySelector(id).scrollIntoView({ behavior: 'smooth' });
//   });
// });

// EVENT LISTENER:
// On nav links click -> scroll to section
// Event delegation
// 1. Add event listener to common parent element
// 2. Determine what element originated the event
document.querySelector('.nav__links').addEventListener('click', function (e) {
  e.preventDefault();
  // Matching strategy

  // Guard clause
  // If clicked element !== nav__link -> return
  if (!e.target.classList.contains('nav__link')) return;

  const id = e.target.getAttribute('href');
  document.querySelector(id).scrollIntoView({ behavior: 'smooth' });
});

///// Fade animation
// FUNCTION:
const handleHover = function (e) {
  // Guard clause
  // CASE: event happened on element !== .nav__link element
  if (!e.target.classList.contains('nav__link')) return;

  const link = e.target;
  const siblings = link.closest('nav').querySelectorAll('.nav__link');
  const logo = link.closest('nav').querySelector('img');

  siblings.forEach(el => {
    // CASE: not the target element itself
    if (el !== link) el.style.opacity = this; // as if we had opacity passed as argument
  });
  logo.style.opacity = this;
};

// Passing "argument" into handler
// EVENT LISTENER:
// On nav link hover -> manipulate opacity
nav.addEventListener('mouseover', handleHover.bind(0.5)); // this for handleHover()
nav.addEventListener('mouseout', handleHover.bind(1));

/////// Sticky nav: Intersection Observer API
// On page reaching 1st section -> stick navigation to top
const navHeight = nav.getBoundingClientRect().height;

// FUNCTION:
// headerObserver callback ()
const stickyNav = function (entries) {
  const [entry] = entries;

  // CASE: given element not visible
  if (!entry.isIntersecting) nav.classList.add('sticky');
  // CASE: given element visible
  else nav.classList.remove('sticky');
};

const headerObserver = new IntersectionObserver(stickyNav, {
  rot: null,
  threshold: 0,
  rootMargin: `-${navHeight}px`,
});

headerObserver.observe(header);

///////////////////////////////////////
// Sections reveal
const allSections = document.querySelectorAll('.section');

// FUNCTION:
// sectionObserver callback ()
const revealSection = function (entries, observer) {
  const [entry] = entries;

  // Guard clause
  if (!entry.isIntersecting) return;

  entry.target.classList.remove('section--hidden');
  observer.unobserve(entry.target);
};

const sectionObserver = new IntersectionObserver(revealSection, {
  root: null,
  threshold: 0.15,
});

allSections.forEach(section => {
  sectionObserver.observe(section);
  section.classList.add('section--hidden');
});

///////////////////////////////////////
// Lazy loading images
const imgTargets = document.querySelectorAll('img[data-src]');

// FUNCTION:
// imgObserver callback ()
const loadImg = function (entries, observer) {
  const [entry] = entries;

  // Guard clause
  if (!entry.isIntersecting) return;

  // Replace src img
  entry.target.src = entry.target.dataset.src;
  // Remove blur filter on load done
  entry.target.addEventListener('load', function () {
    entry.target.classList.remove('lazy-img');
  });
  observer.unobserve(entry.target);
};

const imgObserver = new IntersectionObserver(loadImg, {
  root: null,
  threshold: 0,
  rootMargin: '200px',
});

imgTargets.forEach(img => imgObserver.observe(img));

///////////////////////////////////////
// Tabbed component

// EVENT LISTENER:
// On tabs click
tabsContainer.addEventListener('click', function (e) {
  // Get button in case click happened on any child element
  const clicked = e.target.closest('.operations__tab');

  // Guard clause
  // CASE: clicked element !== .operations__tab
  if (!clicked) return;

  // Remove active classes
  tabs.forEach(t => t.classList.remove('operations__tab--active'));
  tabsContent.forEach(c => c.classList.remove('operations__content--active'));

  // Active tab
  clicked.classList.add('operations__tab--active');

  // Activate content area
  document
    .querySelector(`.operations__content--${clicked.dataset.tab}`)
    .classList.add('operations__content--active');
});

///////////////////////////////////////
// Slider component
// FUNCTION:
// Function to encapsulate slider
const slider = function () {
  const slides = document.querySelectorAll('.slide');
  const btnPrev = document.querySelector('.slider__btn--left');
  const btnNext = document.querySelector('.slider__btn--right');
  const dotContainer = document.querySelector('.dots');

  let curSlide = 0;
  const maxSlide = slides.length - 1;

  // FUNCTION:
  // Function to align slides next to each other
  const goToSlide = function (slide) {
    slides.forEach(
      (s, i) => (s.style.transform = `translateX(${100 * (i - slide)}%)`)
      //0, 100%, 200%.... 0, -100%, -200%
    );
  };

  // FUNCTION:
  // Function to go to the next slide
  const nextSlide = function () {
    if (curSlide === maxSlide) {
      curSlide = 0;
    } else {
      curSlide++;
    }
    goToSlide(curSlide);
    activeDot(curSlide);
  };

  // FUNCTION:
  // Function to go to the previous slide
  const prevSlide = function () {
    if (curSlide === 0) {
      curSlide = maxSlide;
    } else {
      curSlide--;
    }
    goToSlide(curSlide);
    activeDot(curSlide);
  };

  // FUNCTION:
  // Function to create page dots
  const createDots = function () {
    slides.forEach((_slide, index) => {
      dotContainer.insertAdjacentHTML(
        'beforeend',
        `<button class="dots__dot" data-slide="${index}"></button>`
      );
    });
  };

  // FUNCTION:
  // Function to highlight the active dot
  const activeDot = function (slide) {
    document
      .querySelectorAll('.dots__dot')
      .forEach(dot => dot.classList.remove('dots__dot--active'));

    document
      .querySelector(`.dots__dot[data-slide="${slide}"]`)
      .classList.add('dots__dot--active');
  };

  // FUNCTION:
  // Function to init the slider
  const init = function () {
    // go to 1st slide of the slider
    goToSlide(0);
    // create slider pagination
    createDots();
    // highlight the first slide as active
    activeDot(0);
  };
  init();

  // EVENT LISTENERS:
  // On next / prev click -> change slide
  btnNext.addEventListener('click', nextSlide);
  btnPrev.addEventListener('click', prevSlide);
  // On arrLeft / arrRight click -> change slide
  document.querySelector('keydown', function (e) {
    e.key === 'ArrowLeft' && prevSlide();
    e.key === 'ArrowRight' && nextSlide();
  });

  dotContainer.addEventListener('click', function (e) {
    if (e.target.classList.contains('dots__dot')) {
      const { slide } = e.target.dataset; //const slide = e.target.dataset.slide;
      goToSlide(slide);
      activeDot(slide);
    }
  });
};
slider();

///////////////////////////////////////
// MISC

// EVENT LISTENER:
// On learn more click -> scroll to section
btnScrollTo.addEventListener('click', e => {
  // Scroll to section 1

  // Old-school way
  // Get section--1 position
  // const s1coords = section1.getBoundingClientRect();
  // Scroll to it [ABSOLUTE position aka works even when pageYoffset != 0]
  // window.scrollTo(
  //   s1coords.left + window.pageXOffset,
  //   s1coords.top + window.pageYOffset
  // );
  // window.scrollTo({
  //   left: s1coords.left + window.pageXOffset,
  //   top: s1coords.top + window.pageYOffset,
  //   behavior: 'smooth',
  // });

  // Modern js
  section1.scrollIntoView({ behavior: 'smooth' });
});

// Add cookie message
// const message = document.createElement('div');
// message.classList.add('cookie-message');
// message.innerHTML = `We use cookies to ensure that we give you the best experience on our website.
//   <button class="btn btn--cookie">Got it!</button>`;
// header.prepend(message);

// // EVENT LISTENER:
// // ON cookie info close
// document.querySelector('.btn--cookie').addEventListener('click', e => {
//   e.preventDefault();
//   message.remove();
// });
