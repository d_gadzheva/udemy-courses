'use strict';

/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

//FUNCTIONs below this line:

//FUNCTION to generate a random number between 1 and 6
const rollDice = () => Math.trunc(Math.random() * 6) + 1;

//FUNCTION to switch players
const switchPlayer = () => {
  //Reset current score for the player &
  //Switch the player & background
  currentScore = 0;
  document.getElementById(`current--${activePlayer}`).textContent = 0;
  activePlayer = activePlayer === 0 ? 1 : 0;
  selectPlayer0.classList.toggle('player--active');
  selectPlayer1.classList.toggle('player--active');
};

//FUNCTION to START OR RESET the game
const init = () => {
  //Assign value to variables
  selectTargetScore.disabled = false;
  selectTargetScore.value = 100;
  playUntil = targetScore;
  console.log(playUntil + ' playUntil from init()');
  scores = [0, 0];
  currentScore = 0;
  activePlayer = 0;
  countdown = 3;
  playing = true;
  //Reset all score fields
  selectScore0.textContent = 0;
  selectScore1.textContent = 0;
  selectCurrent0.textContent = 0;
  selectCurrent1.textContent = 0;
  //Reset classes;
  selectPlayer0.classList.add('player--active');
  selectPlayer1.classList.remove('player--active');
  selectPlayer0.classList.remove('player--winner');
  selectPlayer1.classList.remove('player--winner');
  //Reset buttons
  selectRoll.disabled = false;
  selectHold.disabled = false;
  selectRoll.classList.remove('wait');
  selectHold.classList.remove('wait');
  //Hide the dice & countdown
  selectDice.classList.add('hidden');
  selectCountdown.classList.add('hidden');
  //Reset player's names if left empty
  if (selectName0.value === '') selectName0.value = 'Player 1';
  if (selectName1.value === '') selectName1.value = 'Player 2';
};

//FUNCTION to close modal
const closeModal = () => {
  selectModal.classList.add('hidden');
  selectOverlay.classList.add('hidden');
};

//FUNCTION to show modal
const showModal = () => {
  selectModal.classList.remove('hidden');
  selectOverlay.classList.remove('hidden');
};

//Variables below this line:

//Variables for the players
const selectPlayer0 = document.querySelector('.player--0');
const selectPlayer1 = document.querySelector('.player--1');
const selectName0 = document.getElementById('name--0');
const selectName1 = document.getElementById('name--1');
//Variables for dice & score
const selectDice = document.querySelector('.dice');
const selectScore0 = document.getElementById('score--0');
const selectScore1 = document.getElementById('score--1');
const selectCurrent0 = document.getElementById('current--0');
const selectCurrent1 = document.getElementById('current--1');
const selectTargetScore = document.querySelector('.target-score');
let targetScore = +selectTargetScore.value;
//Variables for the buttons
const selectNew = document.querySelector('.btn--new');
const selectRoll = document.querySelector('.btn--roll');
const selectHold = document.querySelector('.btn--hold');
let buttons = [selectRoll, selectHold, selectNew];
//Variables for the countdown
const selectCountdown = document.querySelector('.countdown');
const selectCountdownNumber = document.querySelector('.countdown-number');
const selectCountdownLabel = document.querySelector('.countdown-label');
//Variables for the modal
const selectModal = document.querySelector('.modal');
const selectOverlay = document.querySelector('.overlay');
const selectCloseModal = document.querySelector('.close-modal');
const selectShowModal = document.querySelector('.show-modal');
//Variables for the init function
let playUntil, scores, currentScore, activePlayer, countdown, playing;

//Initialize the game
init();

//NOTES
//Logic behind PLAY UNTIL below this line:

// EVENT LISTENER:
//On PLAY UNTIL change
selectTargetScore.addEventListener('input', () => {
  //Update playUntil to input's value
  if (+selectTargetScore.value > 1) playUntil = +selectTargetScore.value;

  //FIXME development only
  console.log(playUntil + ' playUntil on input change if input > 1');
});

//NOTES
//Logic behind ROLL button below this line:

// EVENT LISTENER:
//On btn--roll click
selectRoll.addEventListener('click', () => {
  if (playing) {
    //Hide countdown if visible &
    //Hide dice if visible
    if (!selectCountdown.classList.contains('hidden'))
      selectCountdown.classList.add('hidden');
    if (selectDice.classList.contains('hidden'))
      selectDice.classList.remove('hidden');

    //Disable changing the target once the game begins &
    //Reset play until value to 100 in case user inputed incorrect value [less than 2]
    selectTargetScore.disabled = true;
    if (+selectTargetScore.value < 2) {
      selectTargetScore.value = 100;

      //FIXME development only
      console.log(playUntil + ' playUntil from ROLL click if input < 2');
    }

    //Reset players names if left empty
    if (selectName0.value === '') selectName0.value = 'Player 1';
    if (selectName1.value === '') selectName1.value = 'Player 2';

    //Roll the dice
    const dice = rollDice();

    //FIXME development only
    console.log(`Play until ${playUntil}`);
    console.log(dice);

    //Select the right dice image &
    //Display it to the user
    selectDice.src = `dice-${dice}.png`;
    selectDice.classList.remove('hidden');

    //CASE dice is != 1 -> update score
    if (dice !== 1) {
      //Add dice to current score &
      //Display it to the user
      currentScore += dice;
      document.getElementById(
        `current--${activePlayer}`
      ).textContent = currentScore;

      //CASE dice is 1 -> switch players
    } else {
      //NOTES
      //Logic to COUNTDOWN animation below

      //Reset countdown & show it
      countdown = 3;
      selectCountdownNumber.textContent = countdown;
      selectCountdownLabel.textContent = `${
        activePlayer === 1 ? selectName0.value : selectName1.value
      } in`;
      selectCountdown.classList.remove('hidden');

      //Disable BUTTONS to prevent accidental dice roll from wrong player
      buttons.forEach(button => {
        button.disabled = true;
        button.classList.add('wait');
      });

      //Display the countdown to the user
      let runCountdown = setInterval(() => {
        //CASE countdown between 3 and 1
        if (countdown) {
          //Decrease it by one
          selectCountdownNumber.textContent = countdown--;

          //CASE countdown is 0
        } else {
          selectCountdownNumber.textContent = 'Go!';
          //Stop interval from running
          clearInterval(runCountdown);
          //Switch players
          switchPlayer();
          //Enable BUTTONS again
          buttons.forEach(button => {
            button.disabled = false;
            button.classList.remove('wait');
          });
          //Hide dice and prompt next player to act
          selectDice.classList.add('hidden');
          selectCountdownLabel.textContent =
            activePlayer === 1 ? selectName1.value : selectName0.value;
        }
      }, 1000);
    }
  }
});

//NOTES
//Logic behind HOLD button below this line:

// EVENT LISTENER:
//On btn--hold click
selectHold.addEventListener('click', () => {
  if (playing) {
    //Hide dice & countdown
    selectDice.classList.add('hidden');
    selectCountdown.classList.add('hidden');
    //Add current score to active player's total score &
    //Display it to the user
    scores[activePlayer] += currentScore;
    document.getElementById(`score--${activePlayer}`).textContent =
      scores[activePlayer];

    //Check if score >= playUntil
    if (scores[activePlayer] >= playUntil) {
      //Finish the game
      playing = false;
      //Highlight winner
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.add('player--winner');
      document
        .querySelector(`.player--${activePlayer}`)
        .classList.remove('player--active');
      document.getElementById(`score--${activePlayer}`).textContent = `Winner!`;
      //Disable ROLL / HOLD buttons
      selectRoll.disabled = true;
      selectHold.disabled = true;
      selectRoll.classList.add('wait');
      selectHold.classList.add('wait');
    } else {
      //Switch players
      switchPlayer();
    }
  }
});

//NOTES
//Logic behind NEW GAME button below this line:

// EVENT LISTENER:
//On btn--new click
selectNew.addEventListener('click', init);

// EVENT LISTENER:
//Show modal on click
selectShowModal.addEventListener('click', showModal);

// EVENT LISTENER:
//Close modal on CLOSE-MODAL OR OVERLAY click
selectCloseModal.addEventListener('click', closeModal);
selectOverlay.addEventListener('click', closeModal);

// EVENT LISTENER:
//Handle ESC key
document.addEventListener('keydown', function (e) {
  //If MODAL is visible - close it
  if (e.key === 'Escape' && !selectModal.classList.contains('hidden'))
    closeModal();
});

// EVENT LISTENER:
//Select names inputs on focus
document.addEventListener('click', event => {
  if (event.target !== selectName0 && event.target !== selectName1) return;

  event.target.select();
});
