export const API_URL = `https://forkify-api.herokuapp.com/api/v2/recipes/`;
export const TIMEOUT_SEC = 10;
export const RESULTS_PER_PAGE = 10;
export const KEY = `d27c08a1-1cbf-48f0-80a9-72b241a9cf0f`;
export const CLOSE_MODAL_SEC = 2.5;
