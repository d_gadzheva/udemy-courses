// IMPORTS:
import * as model from './model.js';
import { CLOSE_MODAL_SEC } from './config.js';
import recipeView from './views/recipeView.js';
import searchView from './views/searchView.js';
import searchResultsView from './views/searchResultsView.js';
import paginationView from './views/paginationView.js';
import addRecipeView from './views/addRecipeView.js';

import 'core-js/stable';
import 'regenerator-runtime/runtime';
import bookmarksView from './views/bookmarksView.js';

// VARIABLES:
const recipeContainer = document.querySelector('.recipe');

// FUNCTION:
// Function to render recipe to UI
const controlRecipes = async function () {
  try {
    // Get recipe id from url hash
    const id = window.location.hash.slice(1);

    // CASE: Guard clause -> No recipe id
    if (!id) return;

    // Show loader
    recipeView.renderLoader();

    // 0. Update results view to mark selected search result
    searchResultsView.update(model.getSearchResultsPage());

    // 1. Updating bookmarks view
    bookmarksView.update(model.state.bookmarks);

    // 2. Load recipe [get recipe from model]
    await model.loadRecipe(id);

    // 3. Render recipe to UI
    recipeView.render(model.state.recipe);

    // Error handling
  } catch (err) {
    // FIXME: test only
    console.error(err);
    // Render error to UI
    recipeView.renderError();
  }
};

// FUNCTION:
// Function to render search results to UI
const controlSearchResults = async function () {
  try {
    // Get query
    const query = searchView.getQuery();

    // CASE: Guard clause -> No search input
    if (!query) return;

    // Show loader
    searchResultsView.renderLoader();

    // 2. Load search results [get results from model]
    await model.loadSearchResults(query);

    // 3. Render search results to UI
    searchResultsView.render(model.getSearchResultsPage());

    // 4. Render pagination to UI
    paginationView.render(model.state.search);
  } catch (err) {
    // FIXME: test only
    console.error(err);
    // Render error to UI
    recipeView.renderError();
  }
};
controlSearchResults();

// FUNCTION:
// Function to render pagination to UI
const controlPagination = function (goToPage) {
  // 1. Render NEW search results to UI
  searchResultsView.render(model.getSearchResultsPage(goToPage));

  // 2. Render NEW pagination to UI
  paginationView.render(model.state.search);
};

// FUNCTION:
// Function to update servings
const controlServings = function (newServings) {
  // 1. Update servings
  model.updateServings(newServings);

  // 2. Update recipe view
  recipeView.update(model.state.recipe);
};

// FUNCTION:
// Function to add recipe to bookmark
const controlAddBookmark = function () {
  // 1. Add/ Remove bookmark
  // CASE: Add bookmark
  if (!model.state.recipe.bookmarked) model.addBookmark(model.state.recipe);
  // CASE: Remove bookmark
  else model.deleteBookmark(model.state.recipe.id);

  // 2. Update recipe view
  recipeView.update(model.state.recipe);

  // 3. Update bookmarks
  bookmarksView.render(model.state.bookmarks);
};

// FUNCTION:
// Function to load bookmarks at start
const controlBookmarks = function () {
  bookmarksView.render(model.state.bookmarks);
};

// FUNCTION:
// Function to get data from add recipe modal
const controlAddRecipe = async function (newRecipe) {
  try {
    // Render loader
    addRecipeView.renderLoader();
    // Upload newRecipe to api
    await model.uploadRecipe(newRecipe);
    console.log(model.state.recipe);
    // Render recipe
    recipeView.render(model.state.recipe);
    // Render success messgae
    addRecipeView.renderSuccess();
    // Render bookmarks
    bookmarksView.render(model.state.bookmarks);
    // Change url
    window.history.pushState(null, '', `${modal.state.recipe.id}`);
    // Close modal
    setTimeout(function () {
      addRecipeView.toggleModal();
    }, CLOSE_MODAL_SEC * 1000);
  } catch (err) {
    addRecipeView.renderError(err.message);
  }
};

// FUNCTION:
// Function initialize forkify
// Subscribes controller to view
// [connect controller and view on initialization so the controller can react to events coming from the view]
const init = function () {
  bookmarksView.addHandlerRender(controlBookmarks);
  recipeView.addHandlerRender(controlRecipes);
  recipeView.addHandlerUpdateServings(controlServings);
  recipeView.addHandlerAddBookmark(controlAddBookmark);
  searchView.addHandlerSearch(controlSearchResults);
  paginationView.addHandlerClick(controlPagination);
  addRecipeView.addHandlerUpload(controlAddRecipe);
};
init();
