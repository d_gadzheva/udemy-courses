// IMPORTS:
import View from './view.js';
import previewView from './previewView.js';

class SearchResultsView extends View {
  _parentElement = document.querySelector('.bookmarks');
  _errorMessage =
    'No bookmarks yet. Start by adding recipes to your bookmarks! 🙃';

  // METHOD:
  // [Publisher for constrolRecipes()]
  addHandlerRender(handler) {
    // EVENT LISTENER:
    // Load bookmarks on page load
    window.addEventListener('load', handler);
  }

  // METHOD:
  // Method to generate search results markup
  _generateMarkup() {
    return this._data
      .map(bookmark => previewView.render(bookmark, false))
      .join('');
  }
}
// EXPORTS:
export default new SearchResultsView();
