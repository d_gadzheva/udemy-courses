// IMPORTS:
import View from './view.js';

class SearchView extends View {
  _parentElement = document.querySelector('.search');

  // METHODL
  // Method to get search input
  getQuery() {
    const query = this._parentElement.querySelector('.search__field').value;
    this._clearInput();
    return query;
  }

  // METHODL
  // Method to clear search input
  _clearInput() {
    this._parentElement.querySelector('.search__field').value = '';
  }

  // METHOD:
  // [Publisher for constrolRecipes()]
  addHandlerSearch(handler) {
    // EVENT LISTENER:
    this._parentElement.addEventListener('submit', function (e) {
      // Prevent page reload
      e.preventDefault();
      handler();
    });
  }
}
// EXPORTS:
export default new SearchView();
