// IMPORTS:
import View from './view.js';
import previewView from './previewView.js';

class AddRecipeView extends View {
  _parentElement = document.querySelector('.upload');
  _message = 'Recipe was successfully uploaded!';

  _window = document.querySelector('.add-recipe-window');
  _overlay = document.querySelector('.overlay');
  _btnOpen = document.querySelector('.nav__btn--add-recipe');
  _btnClose = document.querySelector('.btn--close-modal');

  constructor() {
    super();
    this._addHandlerShowModal();
    this._addHandlerHideModal();
  }

  // METHOD:
  // Method to toggle modal window
  toggleModal() {
    this._overlay.classList.toggle('hidden');
    this._window.classList.toggle('hidden');
  }

  // METHOD:
  // Method to show modal on add recipe click
  _addHandlerShowModal() {
    // EVENT LISTENER:
    this._btnOpen.addEventListener('click', this.toggleModal.bind(this));
  }

  // METHOD:
  // To show modal on add recipe click
  _addHandlerHideModal() {
    // EVENT LISTENER:
    this._btnClose.addEventListener('click', this.toggleModal.bind(this));
    this._overlay.addEventListener('click', this.toggleModal.bind(this));
  }

  addHandlerUpload(handler) {
    this._parentElement.addEventListener('submit', function (e) {
      e.preventDefault();

      const dataArr = [...new FormData(this)];
      const data = Object.fromEntries(dataArr);
      handler(data);
    });
  }

  // METHOD:
  // Method to generate markup
  _generateMarkup() {}
}
// EXPORTS:
export default new AddRecipeView();
