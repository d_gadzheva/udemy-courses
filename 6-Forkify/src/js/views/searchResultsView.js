// IMPORTS:
import View from './view.js';
import previewView from './previewView.js';

class SearchResultsView extends View {
  _parentElement = document.querySelector('.results');
  _errorMessage =
    'We could not find results for your search. Please try again! 🙃';

  // METHOD:
  // Method to generate search results markup
  _generateMarkup() {
    return this._data.map(result => previewView.render(result, false)).join('');
  }
}
// EXPORTS:
export default new SearchResultsView();
