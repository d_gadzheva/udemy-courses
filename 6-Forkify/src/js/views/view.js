// IMPORTS:
import icons from 'url:../../img/icons.svg';

/**
 * Class representing a view
 */
export default class View {
  _data;

  // METHOD:
  /**
   * Render the received object to the DOM (e.g. a single pizza recipe)
   * @param {Object | Object[]} data The data to be rendered (e.g. recipe)
   * @param {boolean} [render=true] If false, create markup string instead of rendering to the DOM
   * @returns {undefined | string} A markup string is returned if render=false
   */
  render(data, render = true) {
    // Render error if no data or empty []
    if (!data || (Array.isArray(data) && data.length === 0))
      return this.renderError();
    // Get data
    this._data = data;
    // Get markup
    const markup = this._generateMarkup();

    // CASE: render called by previewView -> return just the markup string
    if (!render) return markup;

    // Clear parent container and append markup
    this._clear();
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  // METHOD:
  // Method to rerender only changed text / attributes
  // a.k.a Pure shenanigans
  update(data) {
    this._data = data;
    const newMarkup = this._generateMarkup();

    const newDOM = document.createRange().createContextualFragment(newMarkup);
    const newElements = Array.from(newDOM.querySelectorAll('*'));
    const curElements = Array.from(this._parentElement.querySelectorAll('*'));

    newElements.forEach((newEl, i) => {
      const curEl = curElements[i];

      // CASE: changed TEXT
      if (
        !newEl.isEqualNode(curEl) &&
        newEl.firstChild?.nodeValue.trim() !== ''
      ) {
        curEl.textContent = newEl.textContent;
      }

      // CASE: changed ATTRIBUES
      if (!newEl.isEqualNode(curEl))
        Array.from(newEl.attributes).forEach(attr =>
          curEl.setAttribute(attr.name, attr.value)
        );
    });
  }

  // METHOD:
  // Method to render loader
  renderLoader() {
    const markup = `
      <div class="spinner">
        <svg>
          <use href="${icons}#icon-loader"></use>
        </svg>
      </div> 
  `;
    this._clear();
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  // METHOD:
  // Method to render error message
  // Accepts message if needed
  renderError(message = this._errorMessage) {
    const markup = `
        <div class="error">
          <div>
            <svg>
              <use href="${icons}#icon-alert-triangle"></use>
            </svg>
          </div>
          <p>${message}</p>
        </div>
      `;
    this._clear();
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  // METHOD:
  // Method to render success message
  renderSuccess(message = this._message) {
    const markup = `
      <div class="message">
        <div>
          <svg>
            <use href="${icons}#icon-smile"></use>
          </svg>
        </div>
        <p>${message}</p>
      </div>
    `;
    this._clear();
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  // METHOD:
  // Method to clear parent container
  _clear() {
    this._parentElement.innerHTML = '';
  }
}
