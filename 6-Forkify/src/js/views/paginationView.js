import View from './view.js';
import icons from 'url:../../img/icons.svg';

class PaginationView extends View {
  _parentElement = document.querySelector('.pagination');

  // METHOD:
  // [Publisher for constrolRecipes()]
  addHandlerClick(handler) {
    // EVENT LISTENER:
    // Render recipes on hash change / page with hash load
    this._parentElement.addEventListener('click', function (e) {
      const btn = e.target.closest('.btn--inline');

      // CASE Guard clause -> no button clicked
      if (!btn) return;

      const goToPage = +btn.dataset.goto;
      console.log(btn, goToPage);
      handler(goToPage);
    });
  }

  // METHOD
  // Method to generate pagination markup
  _generateMarkup() {
    const curPage = this._data.page;
    const numPages = Math.ceil(
      this._data.results.length / this._data.resultsPerPage
    );
    console.log(curPage, numPages);

    // CASE: 1. Page 1 of many
    // Current page is 1 and total pages are > 1
    if (curPage === 1 && numPages > 1) {
      return this._generateButtonMarkup('next', curPage);
    }
    // CASE: 2. Last page
    // Current page is the last page and total pages are > 1
    if (curPage === numPages && numPages > 1) {
      return this._generateButtonMarkup('prev', curPage);
    }
    // CASE: 3. Middle page of many
    // Current page is < total pages [total pages ARE > 1 if this line reached]
    if (curPage < numPages) {
      return [
        this._generateButtonMarkup('prev', curPage),
        this._generateButtonMarkup('next', curPage),
      ].join('');
    }
    // CASE: 4. Only 1 page [no other possibilities left if this line reached]
    return '';
  }

  // METHOD
  // Method to generate pagination button markup
  // Accepts type 'next' OR 'prev' and current page
  _generateButtonMarkup(type, currentPage) {
    return `
        <button data-goto="${
          type === 'prev' ? currentPage - 1 : currentPage + 1
        }" class="btn--inline pagination__btn--${type}">
            <svg class="search__icon">
              <use href="${icons}#icon-arrow-${
      type === 'prev' ? 'left' : 'right'
    }"></use>
            </svg>
            <span>Page ${
              type === 'prev' ? currentPage - 1 : currentPage + 1
            }</span>
        </button>
    `;
  }
}
// EXPORTS:
export default new PaginationView();
