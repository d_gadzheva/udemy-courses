// IMPORTS:
import { TIMEOUT_SEC } from './config.js';

// FUNCTION:
/**
 * Helper function to reject a promise after given time
 * @param {Number} seconds
 * @returns Promise
 */
const timeout = function (s) {
  return new Promise(function (_, reject) {
    setTimeout(function () {
      reject(new Error(`Request took too long! Timeout after ${s} second`));
    }, s * 1000);
  });
};

// FUNCTION:
/**
 * Helper function to get / send data from / to api
 * @param {String} url
 * @param {Object} [uploadData] -> Recipe to be uploaded
 * @returns
 */
export const AJAX = async function (url, uploadData = undefined) {
  try {
    // Get response grom given url
    const fetchPro = uploadData
      ? fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(uploadData),
        })
      : fetch(url);
    // Timeout promise after given time
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);

    // Convert response to json
    const data = await res.json();

    // Throw error if response.ok !== ok
    if (!res.ok) throw new Error(`${data.message} (${res.status})`);

    // Return data
    return data;

    // Error handling
  } catch (err) {
    // Rethrow error to handle it in caller function
    throw err;
  }
};
