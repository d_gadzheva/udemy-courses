// IMPORTS:
// import { map } from 'core-js/core/array';
import { async } from 'regenerator-runtime';
import { API_URL, RESULTS_PER_PAGE, KEY } from './config.js';
import { AJAX } from './helpers.js';

export const state = {
  recipe: {},
  search: {
    query: '',
    results: [],
    page: 1,
    resultsPerPage: RESULTS_PER_PAGE,
  },
  bookmarks: [],
};

// FUNCTION:
/**
 * Function to create a recipe object from given data
 * @param {Object} data
 * @returns {Object} recipe
 */
const createRecipeObject = function (data) {
  const { recipe } = data.data;
  return {
    id: recipe.id,
    title: recipe.title,
    publisher: recipe.publisher,
    sourceURL: recipe.source_url,
    image: recipe.image_url,
    servings: recipe.servings,
    cookingTime: recipe.cooking_time,
    ingredients: recipe.ingredients,
    ...(recipe.key && { key: recipe.key }),
  };
};

// FUNCTION:
/**
 * Function to get a recipe based on id
 * @param {Sting} id
 */
export const loadRecipe = async function (id) {
  try {
    // Get data [get converted to json response from helpers]
    const data = await AJAX(`${API_URL}/${id}?key=${KEY}`);
    // FIXME: test only
    console.log(data);

    // Set state.recipe to the recieved data
    state.recipe = createRecipeObject(data);

    // Set bookmarked property
    if (state.bookmarks.some(bookmark => bookmark.id === id))
      state.recipe.bookmarked = true;
    else state.recipe.bookmarked = false;

    // Error handling
  } catch (err) {
    // FIXME: test only
    console.error(`${err} 🙃`);
    // Rethrow error to handle it in caller function
    throw err;
  }
};

// FUNCTION:
/**
 * Function to search for a recipe
 * @param {String} query e.g 'pizza'
 */
export const loadSearchResults = async function (query) {
  try {
    // Save search quary to state search object for future referance
    state.search.query = query;

    // Get data [get converted to json response from helpers]
    const data = await AJAX(`${API_URL}?search=${query}&key=${KEY}`);
    // FIXME: test only
    console.log(data);

    // Refactor response properties' names and save it to atate search oject
    state.search.results = data.data.recipes.map(recipe => {
      return {
        id: recipe.id,
        title: recipe.title,
        publisher: recipe.publisher,
        image: recipe.image_url,
        ...(recipe.key && { key: recipe.key }),
      };
    });

    // Reset pagination
    state.search.page = 1;
  } catch (err) {
    // FIXME: test only
    console.log(`${err} 🙃`);
    // Rethrow error to handle it in caller function
    throw err;
  }
};

// FUNCTION:
/**
 * Function to devide search results per page
 * @param {Number} page page to go to
 * @returns {Array} results[] with given start and end based on the desired results per page number
 */
export const getSearchResultsPage = function (page = state.search.page) {
  state.search.page = page;

  const start = (page - 1) * RESULTS_PER_PAGE;
  const end = page * RESULTS_PER_PAGE;

  return state.search.results.slice(start, end);
};

// FUNCTION:
/**
 * Function to update servings count
 * @param {Number} newServings
 */
export const updateServings = function (newServings) {
  console.log('newServings');
  console.log(newServings);
  // Update ingridients based on servings
  // newQuantity = oldQuantity * newServings / oldServings
  state.recipe.ingredients.map(
    ing => (ing.quantity = (ing.quantity * newServings) / state.recipe.servings)
  );

  state.recipe.servings = newServings;
};

// FUNCTION:
/**
 * Function to save bookmarks to localStorage
 */
const persistBookmarks = function () {
  localStorage.setItem('bookmarks', JSON.stringify(state.bookmarks));
};

// FUNCTION:
// Accepts a recipe
/**
 * Function to add a recipe to bookmarks
 * @param {Object} recipe
 */
export const addBookmark = function (recipe) {
  // Add recipe to state.bookmarks
  state.bookmarks.push(recipe);

  // Set a property to the recipe
  if (recipe.id === state.recipe.id) state.recipe.bookmarked = true;

  // Save bookmarks to localStorage
  persistBookmarks();
};

// FUNCTION:
/**
 * Function to remove a recipe from bookmarks
 * @param {String} id
 */
export const deleteBookmark = function (id) {
  // Find recipe with id
  const index = state.bookmarks.findIndex(bookmark => bookmark.id === id);

  // Remove recipe from bookmarks
  state.bookmarks.splice(index, 1);

  // Set a property to the recipe
  if (id === state.recipe.id) state.recipe.bookmarked = false;

  // Save bookmarks to localStorage
  persistBookmarks();
};

// FUNCTION:
/**
 * Function to initialize forkify
 * [1. Get bookmarks from localStorage]
 */
const init = function () {
  const storage = localStorage.getItem('bookmarks');
  if (storage) state.bookmarks = JSON.parse(storage);
};
init();

// FUNCTION:
/**
 * Function to upload a recipe to the api
 * @param {Object} newRecipe
 */
export const uploadRecipe = async function (newRecipe) {
  try {
    const ingredients = Object.entries(newRecipe)
      .filter(entry => entry[0].startsWith('ingredient') && entry[1] !== '')
      .map(ing => {
        const ingArr = ing[1].split(',').map(el => el.trim());

        if (ingArr.length !== 3)
          throw new Error(
            'Wrong ingredient format. Please use the correct format. 🙃'
          );
        const [quantity, unit, description] = ingArr;
        return { quantity: quantity ? +quantity : null, unit, description };
      });

    const recipe = {
      title: newRecipe.title,
      source_url: newRecipe.sourceUrl,
      image_url: newRecipe.image,
      publisher: newRecipe.publisher,
      cooking_time: +newRecipe.cookingTime,
      servings: +newRecipe.servings,
      ingredients,
    };

    // Send the new recipe to api
    const data = await AJAX(`${API_URL}?key=${KEY}`, recipe);
    console.log(data);

    // Set state.recipe to the recieved data
    state.recipe = createRecipeObject(data);

    // Bookmark recipe
    addBookmark(state.recipe);
  } catch (err) {
    throw err;
  }
};
