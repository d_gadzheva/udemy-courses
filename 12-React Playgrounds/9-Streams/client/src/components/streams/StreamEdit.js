import React from "react";
import { connect } from "react-redux";
import { editStream, fetchStream } from "../../actions";
import StreamForm from "./StreamForm";

class StreamEdit extends React.Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
  }
  onSubmit = (formValues) => {
    this.props.editStream(this.props.match.params.id, formValues);
  };
  render() {
    if (!this.props.stream) return <div>Loading..</div>;

    if (this.props.stream.userId === this.props.currentUserId) {
      const { title, description } = this.props.stream;
      const initialValues = { title, description };

      return (
        <div>
          <h3>Edit Stream</h3>
          <StreamForm onSubmit={this.onSubmit} initialValues={initialValues} />
        </div>
      );
    }

    return (
      <div>
        <h3>You do not have the permission to edit this stream.</h3>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
    currentUserId: state.auth.userId,
  };
};

export default connect(mapStateToProps, { editStream, fetchStream })(
  StreamEdit
);
