import React from "react";
import { Link } from "react-router-dom";
import Modal from "../Modal";
import history from "../../history";
import { connect } from "react-redux";
import { fetchStream, deleteStream } from "../../actions";

class StreamDelete extends React.Component {
  componentDidMount() {
    this.props.fetchStream(this.props.match.params.id);
    console.log(this.props.stream);
  }

  onDelete() {
    this.props.deleteStream(this.props.match.params.id);
  }

  renderActions() {
    return (
      <React.Fragment>
        <div className="ui negative button" onClick={() => this.onDelete()}>
          Delete
        </div>
        <Link to="/" className="ui button">
          Cancel
        </Link>
      </React.Fragment>
    );
  }

  renderContent() {
    return `Are you sure you want to delete stream with title: ${this.props.stream.title}?`;
  }

  render() {
    if (!this.props.stream) return <div>Loading...</div>;
    if (this.props.stream.userId === this.props.currentUserId)
      return (
        <div>
          Delete
          <Modal
            title="Delete Stream"
            content={this.renderContent()}
            actions={this.renderActions()}
            onDismiss={() => history.push("/")}
          />
        </div>
      );

    return <div>Loading...</div>;
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
    currentUserId: state.auth.userId,
  };
};

export default connect(mapStateToProps, { fetchStream, deleteStream })(
  StreamDelete
);
