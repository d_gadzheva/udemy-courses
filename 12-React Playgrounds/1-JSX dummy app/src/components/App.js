import React from 'react';
import faker from 'faker';
import ComentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
  return (
    <div className="ui container" style={{ paddingTop: '10px' }}>
      <h2 className="ui header">Pending comments</h2>
      <div className="ui comments">
        <ApprovalCard>
          <ComentDetail
            author="Sam"
            timeAgo="1 hour ago"
            content="Great post!"
            avatarSrc={faker.image.image()}
          />
        </ApprovalCard>
        <ApprovalCard>
          <ComentDetail
            author="Alex"
            timeAgo="20 hours ago"
            content="So informative!"
            avatarSrc={faker.image.image()}
          />
        </ApprovalCard>
        <ApprovalCard>
          <ComentDetail
            author="Nico"
            timeAgo="1 day ago"
            content="Outstanding!"
            avatarSrc={faker.image.image()}
          />
        </ApprovalCard>
      </div>
    </div>
  );
};

export default App;
