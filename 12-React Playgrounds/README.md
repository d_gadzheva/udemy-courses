## 1. JSX dummy app

**Purpose**

Dummy comment approve feed built with dummy data provided by [faker API](https://fakerapi.it/en). **Completely static.** The whole purpose of the app is to see JSX in action and learn how to communicate with props. The project had no real structure, so I broke it down into components etc.

**Tools used**

Written on VS Code using React v16.6.3, Create React App and [faker API](https://fakerapi.it/en). Styled with Semantic UI.

[Live demo JSX dummy app](https://1-jsx-dummy-app.vercel.app)

---

## 2. Seasons

**Purpose**

Introducing class-based components, state, and lifecycle methods. The whole idea behind the app is to determine if it is the warm or cold part of the year based on the current month and location. The project had no real structure, so I broke it down into components etc.

**Notes**

To simulate different locations => go to Console => Customize DevTools => Sensors

**Tools used**

Written on VS Code using React v16.6.3 and Create React App.

[Live demo Seasons](https://2-seasons.vercel.app)

---

## 3. Pics

**Purpose**

Learn how to handle user input with forms and events. How to manage lists. What are keys and refs. The idea of the app is to search for images based on query. All images come from [Unsplash's API](https://unsplash.com/).

**Notes**

You get the first 50 images matching the query.

**Tools used**

Written on VS Code using React v16.6.3, Create React App and axios. Styled with Semantic UI and some custom CSS. Images from [Unsplash](https://unsplash.com/).

[Live demo Pics](https://3-pics-six.vercel.app)

---

## 4. Videos

**Purpose**

Practice everything learned until now. The idea of the app is to search for videos based on query. All videos come from [Youtube's API](https://developers.google.com/youtube/v3).

**Notes**

You get the first 5 videos matching the query. The deafult search query is 'cat'.

**Tools used**

Written on VS Code using React v16.6.3, Create React App and axios. Styled with Semantic UI. Videos from [Youtube's API](https://developers.google.com/youtube/v3).

[Live demo Videos](https://vids-eight.vercel.app)

---

## 5. Widgets

**Purpose**

Understanding hooks in React and building custom navigation.

**Notes**

The app includes 4 widgets:

- Accordion - makes use of useState. Static accordion, that is all;
- Search - makes use of useState, useEffect, axios, and [Wikipedia's open API](https://en.wikipedia.org/w/api.php). You can search for articles based on a query. The default search query is 'programming'.
- Dropdown - makes use of useState, useEffect and useRef. Changes text color based on its value.
- Translate - makes use of useState, useEffect, axios, and [Google Translate API](https://cloud.google.com/translate). You can choose to translate custom input to a list of predefined languages.

**Tools used**

Written on VS Code using React v16.6.3, React Hooks, Create React App and axios. Styled with Semantic UI. Articles from [Wikipedia's open API](https://en.wikipedia.org/w/api.php). Translations from [Google Translate API](https://cloud.google.com/translate).

[Live demo Widgets](https://5-widgets-eight.vercel.app)

---

## 6. Videos with Hooks

**Purpose**

Exercise on React Hooks. The exact same app as Videos but built using function components and hooks.

**Tools used**

Written on VS Code using React v16.6.3, React Hooks, Create React App and axios. Styled with Semantic UI. Videos from [Youtube's API](https://developers.google.com/youtube/v3).

[Live demo Videos](https://6-videos-v-hooks.vercel.app)

---

## 7. Songs

**Purpose**

Integrating React with React-Redux.

**Notes**

The idea is for a list of items between which you can switch. The app is plain and static, as its main purpose is to introduce Redux and show the structure of a React project making use of it.

**Tools used**

Written on VS Code using React v16.6.3, React Redux and Create React App.

[Live demo Songs](https://7-songs.vercel.app/)

---

## 8. Blog

**Purpose**

Another simple app to show the basics of async action creators and Redux Thunk. The idea of the app is to show a list of blog posts coming from [jsonPlaceholder API](https://jsonplaceholder.typicode.com/)

**Tools used**

Written on VS Code using React v16.6.3, React Redux, Redux Thunk and Create React App. Data from [jsonPlaceholder API](https://jsonplaceholder.typicode.com/)

[Live demo Blog](https://8-blog.vercel.app)

---

## 9. Streams

**Purpose**

Simplified Twitch-like stream platform. The user can browse and watch streams or create and manage their own streams after authentication. The main goal of the app was to show React Router and how to use React Dev Tools. All forms are handled with Redux Form. Basic concept of REST APIs introduced.

**Notes**

**You cannot really see the videos attached to each stream as they are not stored anywhere.** The only way to watch an actual video is to stream it yourself. The video is available for as long as the stream is live. To do so you need to:

- download the code;
- npm i => npm start in all api, client and rtmpserver folders;
- download and install [OBS Studio](https://obsproject.com/);
- open OBS Studio;
- create new Scene (bottom left corner => click Scenes' + to add a scene). Name does not matter;
- add a Display Source (right next to Scenes => click Sources' + and select Display Capture). Name does not matter. If any red border appears => click and drag it to match video's borders;
- add an Audio Source (right next to Scenes => click Sources' + and select Audio Input Capture). Name does not matter. Select whatever mic you use;
- Configure Stream settings => click Settings (bottom right corner under Controls) => Stream => choose Custom for Service, enter rtmp://localhost/live for Server, and the id of any existing stream as Stream Key. (If you use the streams already presented in the app, then anything between 1 and 7 works.);
- start Streaming (bottom right corner under Controls);
- open the stream you referred as a Stream Key;
- play the video;
- enjoy!

To only use the CRUD operations:

- download the code;
- npm i => npm start in api and client folders.

**Tools used**

Written on VS Code using React v16.6.3, React Redux, Redux Thunk, React Router, Redux Form and Create React App. Authentication by Google. Fake REST API by [json-server](https://github.com/typicode/json-server). RTMP server by [Node Media Server](https://github.com/illuspas/Node-Media-Server);

Live demo Streams - to see this one in action you will have to use some creativity or follow the steps under Notes (:

---

## 10. Translate

**Purpose**

Explore the Context system in React.

**Notes**

**The app is completely static.** There is no functionality whatsoever apart from switching the languages on flag click. The sole purpose of the app is to teach how to use Context instead of Redux.

**Tools used**

Written on VS Code using React v16.6.3, React Context and Create React App.

[Live demo Translate](https://translate-inky.vercel.app)
