import React from 'react';
import SesonDisplay from './SeasonDisplay';
import Loader from './Loader';

class App extends React.Component {
  // constructor(props) {
  //   super(props);

  //   this.state = { lat: null, errorMessage: "" };
  // }

  state = { lat: null, errorMessage: '' };

  componentDidMount() {
    window.navigator.geolocation.getCurrentPosition(
      position => this.setState({ lat: position.coords.latitude }),
      err => this.setState({ errorMessage: err.message })
    );
  }

  renderContent() {
    if (this.state.errorMessage && !this.state.lat)
      return (
        <div className="ui container" style={{ paddingTop: '10px' }}>
          <div
            className="ui negative message"
            style={{ width: '25rem', margin: '0 auto' }}
          >
            <div className="header">Oops, something went wrong.</div>
            <p>{this.state.errorMessage}</p>
          </div>
        </div>
      );

    if (!this.state.errorMessage && this.state.lat)
      return <SesonDisplay lat={this.state.lat} />;

    return <Loader message="Please accept location request" />;
  }

  render() {
    return <div>{this.renderContent()}</div>;
  }
}

export default App;
