import { combineReducers } from "redux";
import postsReducer from "./postsReducer";
import usersReducer from "./usersReducer";

export default combineReducers({
  // replaceMe: () => "potato",
  posts: postsReducer,
  users: usersReducer,
});
