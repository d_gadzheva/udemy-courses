import React from 'react';

class SearchBar extends React.Component {
  state = { searchQuery: '' };

  onFormSubmit = (e) => {
    e.preventDefault();

    this.props.onSubmit(this.state.searchQuery);
  };

  render() {
    return (
      <div className="ui segment custom-segment">
        <form className="ui inverted form" onSubmit={this.onFormSubmit}>
          <div className="field">
            <label>Search for Images</label>
            <input
              type="text"
              value={this.state.searchQuery}
              onChange={(e) => this.setState({ searchQuery: e.target.value })}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
