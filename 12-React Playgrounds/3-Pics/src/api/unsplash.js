import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID 1P7MDyyx9kdfcr6gtKEUJllH8XOjJ7KfmVQmwqwi3-s',
  },
});
