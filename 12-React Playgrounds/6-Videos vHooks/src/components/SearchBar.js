import React, { useState } from "react";

const SearchBar = ({ onFormSubmit }) => {
  const [searchQuery, setSearchQuery] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();

    console.log("Search bar form submit");
    onFormSubmit(searchQuery);
  };

  const onInputChange = (event) => setSearchQuery(event.target.value);

  return (
    <div className="search-bar ui segment">
      <form className="ui form" onSubmit={onSubmit}>
        <div className="field">
          <label>Search for video:</label>
          <input type="text" value={searchQuery} onChange={onInputChange} />
        </div>
      </form>
    </div>
  );
};

export default SearchBar;
