import { useState, useEffect } from "react";
import youtube from "../apis/youtube";

const useVideos = (defaultSearchQuery) => {
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    search(defaultSearchQuery);
  }, [defaultSearchQuery]);

  const search = async (searchQuery) => {
    const response = await youtube.get("/search", {
      params: { q: searchQuery },
    });

    setVideos(response.data.items);
  };

  return [videos, search];
};

export default useVideos;
