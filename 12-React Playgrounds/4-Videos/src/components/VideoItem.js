import "./VideoItem.css";
import React from "react";

const VideoItem = ({ video, onVideoSelect }) => {
  const { title, description, thumbnails } = video.snippet;
  return (
    <div className="video-item item" onClick={() => onVideoSelect(video)}>
      <img
        src={thumbnails.default.url}
        className="ui small image"
        alt={title}
      />
      <div className="content">
        <div className="header">{title}</div>
      </div>
    </div>
  );
};

export default VideoItem;
