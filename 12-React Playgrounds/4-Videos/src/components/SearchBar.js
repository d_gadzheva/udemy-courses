import React from "react";

class SearchBar extends React.Component {
  state = { searchQuery: "" };

  onFormSubmit = (e) => {
    e.preventDefault();

    console.log("Search bar form submit");
    this.props.onFormSubmit(this.state.searchQuery);
  };

  onInputChange = (event) => this.setState({ searchQuery: event.target.value });

  render() {
    return (
      <div className="search-bar ui segment">
        <form className="ui form" onSubmit={this.onFormSubmit}>
          <div className="field">
            <label>Search for video:</label>
            <input
              type="text"
              value={this.state.searchQuery}
              onChange={this.onInputChange}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
