import axios from "axios";

const KEY = "AIzaSyAkngt4AQa5dHZYM3RcAeVBh_ce0G_f9Qk";

export default axios.create({
  baseURL: "https://www.googleapis.com/youtube/v3",
  params: {
    part: "snippet",
    type: "video",
    maxResults: 5,
    key: KEY,
  },
});
