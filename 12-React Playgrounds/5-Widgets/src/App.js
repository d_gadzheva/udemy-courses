import React, { useState } from 'react';
import Accordion from './components/Accordion';
import Search from './components/Search';
import Dropdown from './components/Dropdown';
import Translate from './components/Translate';
import Route from './components/Route';
import Header from './components/Header';

const items = [
  {
    title: 'Question 1',
    content: 'Question 1 content',
  },
  {
    title: 'Question 2',
    content: 'Question 2 content',
  },
  {
    title: 'Question 3',
    content: 'Question 3 content',
  },
];

const options = [
  {
    label: 'Red',
    value: 'red',
  },
  {
    label: 'Green',
    value: 'green',
  },
  {
    label: 'Blue',
    value: 'blue',
  },
  {
    label: 'Orange',
    value: 'orange',
  },
];

const App = () => {
  const [selected, setSelected] = useState(options[0]);

  const textString = selected.value ? `The text is ${selected.value}` : '';

  return (
    <div className="ui container">
      <Header />
      <Route path="/">
        <Accordion items={items} />
      </Route>
      <Route path="/list">
        <Search />
      </Route>
      <Route path="/dropdown">
        <Dropdown
          selected={selected}
          onSelectedChange={setSelected}
          options={options}
          labelText="Select color"
        />
        <span style={{ color: selected.value }}>{textString}</span>
      </Route>
      <Route path="/translate">
        <Translate />
      </Route>
    </div>
  );
};

export default App;
