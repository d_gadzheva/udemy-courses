import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Search = () => {
  const [searchQuery, setSearchQuery] = useState('programming');
  const [debouncedSearchQuery, setDebouncedSearchQuery] = useState(searchQuery);
  const [results, setResults] = useState([]);

  useEffect(() => {
    const debounceTimeout = setTimeout(() => {
      setDebouncedSearchQuery(searchQuery);
    }, 1000);

    // CLEANUP FUNCT:
    // Gets executed next time useEffect is called
    return () => {
      clearTimeout(debounceTimeout);
    };
    // }
  }, [searchQuery]);

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await axios.get(`https://en.wikipedia.org/w/api.php`, {
        params: {
          action: 'query',
          list: 'search',
          origin: '*',
          format: 'json',
          srsearch: debouncedSearchQuery,
        },
      });

      setResults(data.query.search);
    };

    if (debouncedSearchQuery) fetchData();
  }, [debouncedSearchQuery]);

  const renderedResults = results.map((result) => {
    const regex = /(<([^>]+)>)|(&quot;)/gi;
    const cleanSnippet = result.snippet.replace(regex, '');
    return (
      <div key={result.pageid} className="item">
        <div className="right floated content">
          <a
            className="ui button"
            target="blank"
            href={`https://en.wikipedia.org?curid=${result.pageid}`}
          >
            Go
          </a>
        </div>
        <div className="content">
          <div className="header">{result.title}</div>
          <div>{cleanSnippet}</div>
        </div>
      </div>
    );
  });

  return (
    <div>
      <div className="ui form">
        <div className="field">
          <label>Search arcticles:</label>
          <input
            className="input"
            type="text"
            placeholder="e.g. 'programming'"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
        </div>
      </div>
      <div className="ui celled list">{renderedResults}</div>
    </div>
  );
};

export default Search;
