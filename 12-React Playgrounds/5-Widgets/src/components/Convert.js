import axios from "axios";
import React, { useState, useEffect } from "react";

const KEY = "AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM";

const Convert = ({ language, text }) => {
  const [output, setOutput] = useState("");
  const [debouncedOutput, setDebouncedOutput] = useState(text);

  useEffect(() => {
    const fetchTimeout = setTimeout(() => {
      setDebouncedOutput(text);
    }, 1000);
    return () => {
      clearTimeout(fetchTimeout);
    };
  }, [text]);

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await axios.post(
        "https://translation.googleapis.com/language/translate/v2",
        {},
        {
          params: {
            q: debouncedOutput,
            target: language.value,
            key: KEY,
          },
        }
      );

      setOutput(data.data.translations[0].translatedText);
    };

    if (debouncedOutput) fetchData();
  }, [language, debouncedOutput]);
  return (
    <div>
      <h1 className="ui header">{output}</h1>
    </div>
  );
};

export default Convert;
