import { combineReducers } from 'redux';

const songsReducer = () => {
  return [
    { title: 'Song 1', duration: '2:58' },
    { title: 'Song 2', duration: '3:05' },
    { title: 'Song 3', duration: '3:40' },
    { title: 'Song 4', duration: '2:15' },
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') return action.payload;

  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
});
