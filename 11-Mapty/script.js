'use strict';

const form = document.querySelector('.form');
const containerWorkouts = document.querySelector('.workouts');
const inputType = document.querySelector('.form__input--type');
const inputDistance = document.querySelector('.form__input--distance');
const inputDuration = document.querySelector('.form__input--duration');
const inputCadence = document.querySelector('.form__input--cadence');
const inputElevation = document.querySelector('.form__input--elevation');
const containerNotification = document.querySelector('.notification-container');
const closeNotification = document.querySelector('.notification__close');
const sidebar = document.querySelector('.sidebar');
const map = document.querySelector('.js--test');

class Workout {
  date = new Date();
  // Dummy id
  id = (Date.now() + '').slice(-10);

  constructor(coords, distance, duration) {
    this.coords = coords; // [lat, long]
    this.distance = distance; // in km
    this.duration = duration; // in mins
  }

  _setDescription() {
    // prettier-ignore
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    this.description = `${this.type[0].toUpperCase()}${this.type.slice(1)} on ${
      months[this.date.getMonth()]
    } ${this.date.getDate()}`;
  }
}

class Running extends Workout {
  type = 'running';
  constructor(coords, distance, duration, cadance) {
    super(coords, distance, duration);
    this.cadance = cadance;
    this.calcPace();
    this._setDescription();
  }

  calcPace() {
    // min/km
    this.pace = this.duration / this.distance;
    return this.pace;
  }
}

class Cycling extends Workout {
  type = 'cycling';
  constructor(coords, distance, duration, elevationGain) {
    super(coords, distance, duration);
    this.elevationGain = elevationGain;
    this.calcSpeed();
    this._setDescription();
  }

  calcSpeed() {
    // km/h
    this.speed = this.distance / (this.duration / 60);
    return this.speed;
  }
}

////////////////////////////////////////////////
// Application architecture

class App {
  #map;
  #mapZoomLevel = 14;
  #mapEvent;
  #workouts = [];

  constructor() {
    // Get user's position
    this._getPosition();

    // Get data from localStorage
    this._getLocalStorage();

    // EVENT LISTENER:
    // On form submit
    form.addEventListener('submit', this._newWorkout.bind(this));

    // EVENT LISTENER:
    // On select change - switch last input
    inputType.addEventListener('change', this._toggleElevationField);

    // EVENT LISTENER:
    // On workout element click - go to workout on map
    containerWorkouts.addEventListener('click', this._goToPopup.bind(this));

    // EVENT LISTENER:
    // Remove notification on close click
    containerNotification.addEventListener('click', this.notificationClose);

    // EVENT LISTENER:
    // Listen for dynamically added elements
    document.addEventListener(
      'click',
      function (e) {
        // CASE: clicked element !== workout__delete
        if (
          !e.target &&
          !e.target.parentElement.classList.contains('workout__delete')
        )
          return;

        // CASE: Delete workout
        if (e.target.parentElement.classList.contains('workout__delete')) {
          this.notificationPrint(
            'warrning',
            ' Are you sure you want to delete this workout?',
            e.target.closest('.workout')
          );
        }

        // CASE: Confirm delete workout
        if (e.target.getAttribute('data-action') === 'delete') {
          e.preventDefault();
          this._removeWorkout(e.target);
        }

        // CASE: Cancel delete workout
        if (e.target.getAttribute('data-action') === 'cancel')
          e.target.closest('.notification').remove();
      }.bind(this)
    );

    //TODO: place fake marker on initial location select
    //map.addEventListener('click', this._placeFakeMarker.bind(this));
  }

  // Get user position based on navigator.geolocation
  _getPosition() {
    if (navigator.geolocation)
      navigator.geolocation.getCurrentPosition(
        // CASE: user permit to give their position
        this._loadMap.bind(this),
        function () {
          setTimeout(() => {
            // CASE: user denied to give their position
            this.notificationPrint(
              'error',
              'Mapty requires permission to access your location.',
              containerNotification
            );
          }, 3000);
        }.bind(this)
      );
  }

  // Load map with position from navigator.geolocation
  _loadMap(position) {
    const { latitude } = position.coords;
    const { longitude } = position.coords;
    const coords = [latitude, longitude];

    if (document.querySelector('.notification'))
      document.querySelector('.notification').remove();

    this.#map = L.map('map').setView(coords, this.#mapZoomLevel);

    L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.#map);

    // EVENT LISTENER:
    // On map click
    this.#map.on('click', this._showForm.bind(this));

    // Show markers if any data in localStorage
    this.#workouts.forEach((work) => {
      this._renderWorkoutMarker(work);
    });
  }

  // Display form
  _showForm(mapE) {
    this.#mapEvent = mapE;
    form.classList.remove('hidden');
    inputDistance.focus();
  }

  // Hide form
  _hideForm() {
    // Clear inputs
    inputDistance.value = inputDuration.value = inputCadence.value = inputElevation.value =
      '';
    // Hide form animation hacks
    form.style.display = 'none';
    form.classList.add('hidden');
    setTimeout(() => (form.style.display = 'grid'), 1000);
  }

  // Toggle last input field according to workout type
  _toggleElevationField() {
    inputCadence.closest('.form__row').classList.toggle('form__row--hidden');
    inputElevation.closest('.form__row').classList.toggle('form__row--hidden');
  }

  // Create new workout
  _newWorkout(e) {
    e.preventDefault();

    // FUNCTIONS:
    // Helper functions to validate input data
    const validInputs = (...inputs) =>
      inputs.every((inp) => Number.isFinite(inp));
    const AllPositive = (...inputs) => inputs.every((inp) => inp > 0);

    // Get data from form
    const type = inputType.value;
    const distance = +inputDistance.value;
    const duration = +inputDuration.value;
    // Get click coords
    const { lat, lng } = this.#mapEvent.latlng;
    let workout;

    // CASE: workout is running
    // If workout = running -> create running object
    if (type === 'running') {
      const cadance = +inputCadence.value;
      // CASE: invalid data
      // Display error notification
      // Wait for new input
      if (
        !validInputs(distance, duration, cadance) ||
        !AllPositive(distance, duration, cadance)
      ) {
        this.notificationPrint(
          'error',
          'You must enter positive numbers for all fields.',
          containerNotification,
          true
        );
        return;
      }

      // CASE: valid data
      // Push workout to #workouts
      // Display success notification
      workout = new Running([lat, lng], distance, duration, cadance);
      this.notificationPrint(
        'success',
        'You successfully added a new running workout.',
        containerNotification,
        true
      );
    }
    // CASE: workout is cycling
    // If workout = cycling -> create cycling object
    if (type === 'cycling') {
      const elevationGain = +inputElevation.value;
      // CASE: invalid data
      // Display error notification
      // Wait for new input
      if (
        !validInputs(distance, duration, elevationGain) ||
        !AllPositive(distance, duration)
      ) {
        this.notificationPrint(
          'error',
          'You must enter positive numbers for all fields except the elevation gain feild.',
          containerNotification,
          true
        );
        return;
      }

      // CASE: valid data
      // Push workout to #workouts
      // Display success notification
      workout = new Cycling([lat, lng], distance, duration, elevationGain);
      this.notificationPrint(
        'success',
        'You successfully added a new cycling workout.',
        containerNotification,
        true
      );
    }

    // Add the new object to the workout []
    this.#workouts.push(workout);

    // Show  marker
    this._renderWorkoutMarker(workout);

    // Render the workout on the list
    this._renderWorkout(workout);

    // Hide the form
    this._hideForm();

    // Store data in localStorage
    this._setLocalStorage();
  }

  // Render pin to map
  _renderWorkoutMarker(workout) {
    if (document.querySelector('.test'))
      document.querySelector('.test').remove();
    // Set custom pin as map marker
    const customIcon = L.icon({
      iconUrl: 'icon.png',

      iconSize: [50, 50], // size of the icon
      iconAnchor: [26, 75], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 62], // the same for the shadow
      popupAnchor: [-3, -76], // point from which the popup should open relative to the iconAnchor
    });

    // Render map

    L.marker(workout.coords, { icon: customIcon })
      .addTo(this.#map)
      .bindPopup(
        L.popup({
          maxWidth: 250,
          minWidth: 100,
          autoClose: false,
          closeOnClick: false,
          className: `${workout.type}-popup`,
        })
      )
      .setPopupContent(
        `${workout.type === 'running' ? '🏃‍♂️' : '🚴‍♀️'} ${workout.description}`
      )
      .openPopup();
  }

  // Render workout list entry
  _renderWorkout(workout) {
    let html = `
      <li class="workout workout--${workout.type}" data-id="${workout.id}">
        <h2 class="workout__title">${workout.description}</h2>
        <div class="workout__options">  
          <a href="#" class="workout__delete"><ion-icon name="close-outline"></ion-icon></a>
        </div>
        <div class="workout__details">
          <span class="workout__icon">${
            workout.type === 'running' ? '🏃‍♂️' : '🚴‍♀️'
          }</span>
          <span class="workout__value">${workout.distance}</span>
          <span class="workout__unit">km</span>
        </div>
        <div class="workout__details">
          <span class="workout__icon">⏱</span>
          <span class="workout__value">${workout.duration}</span>
          <span class="workout__unit">min</span>
        </div>      
    `;

    // CASE: running
    if (workout.type === 'running')
      html += `
        <div class="workout__details">
          <span class="workout__icon">⚡️</span>
          <span class="workout__value">${workout.pace.toFixed(1)}</span>
          <span class="workout__unit">min/km</span>
        </div>
        <div class="workout__details">
          <span class="workout__icon">🦶🏼</span>
          <span class="workout__value">${workout.cadance}</span>
          <span class="workout__unit">spm</span>
        </div>
      </li>
      `;

    // CASE: cycling
    if (workout.type === 'cycling')
      html += `
        <div class="workout__details">
          <span class="workout__icon">⚡️</span>
          <span class="workout__value">${workout.speed.toFixed(1)}</span>
          <span class="workout__unit">km/h</span>
        </div>
        <div class="workout__details">
          <span class="workout__icon">⛰</span>
          <span class="workout__value">${workout.elevationGain}</span>
          <span class="workout__unit">m</span>
        </div>
      </li>
      `;

    form.insertAdjacentHTML('afterend', html);
  }

  // Focus on map marker
  _goToPopup(e) {
    // CASE: Guard clause -> map not loaded
    if (!this.#map) return;

    const workoutEl = e.target.closest('.workout');

    // CASE: Guard clause -> click not on list item
    if (!workoutEl) return;

    const workout = this.#workouts.find(
      (work) => work.id === workoutEl.dataset.id
    );
    this.#map.setView(workout.coords, this.#mapZoomLevel, {
      animate: true,
      pan: {
        duration: 1,
      },
    });
  }

  // Save data in localStorage
  _setLocalStorage() {
    localStorage.setItem('workouts', JSON.stringify(this.#workouts));
  }

  // Get data from localStorage
  _getLocalStorage() {
    const data = JSON.parse(localStorage.getItem('workouts'));

    // CASE: no data saved in localStorage
    if (!data) return;
    this.#workouts = data;

    this.#workouts.forEach((work) => {
      this._renderWorkout(work);
    });
  }

  // Reset localStorage
  reset() {
    localStorage.removeItem('workouts');
    location.reload();
  }

  /////////////////////////////////
  // Custom js

  // find workout based on workout id
  _findWorkout(target) {
    // e.preventDefault();

    // Return workout with current id
    return this.#workouts.find(
      (workout) =>
        workout.id === target.closest('.workout').getAttribute('data-id')
    );
  }

  // Remove workout from list AND localStorage
  _removeWorkout(target) {
    // Find workout to delete
    const deleteWorkout = this._findWorkout(target);

    // Filter all workouts but the one to be deleted
    this.#workouts = this.#workouts.filter(
      (workout) => workout.id !== deleteWorkout.id
    );
    // Set localStorage to the filtered []
    this._setLocalStorage();
    // Remove the current workout from list
    target.closest('.workout').remove();
    // Reload page
    location.reload();
  }

  // Create notification
  // type = 'success' OR 'error'
  // message = string
  // appendTo = parent element to be appended to
  notificationPrint(type, message, appendTo, timeout = false) {
    // Clear any previous notifications
    containerNotification.innerHTML = '';
    // Notification markup
    const closeBtn = `
      <a href="#" class="notification__close">
        <ion-icon name="close-outline"></ion-icon>
      </a>
    `;
    const actionBtns = `
      <div class="notification__buttons">      
        <span data-action="delete">Yes</span>
        <span data-action="cancel">No</span>
      </div>
    `;

    const html = `
          <div class="notification notification--${type}">
            ${type === 'warrning' ? '' : closeBtn}  
            <span class="notification__icon">${
              type === 'success' ? '🎉' : '⚠️'
            }</span>
            <span class="notification__message">${message}</span>            
            ${type === 'warrning' ? actionBtns : ''}            
          </div>
        `;

    // Where to be inserted
    appendTo.insertAdjacentHTML('afterbegin', html);

    // CASE: notification with timeout
    // Remove the notification after 5s
    if (timeout)
      setTimeout(() => {
        // CASE: successful input before error timer done
        // JS tries to remove a notification which no longer exists
        if (!document.querySelector(`.notification--${type}`)) return;
        document.querySelector(`.notification--${type}`).remove();
      }, 3500);
  }

  // Close notification
  notificationClose(e) {
    e.preventDefault();
    // CASE: notification exists

    if (e.target.parentElement.classList.contains('notification__close'))
      e.target.closest('.notification').remove();
  }
}

const app = new App();
