'use strict';

/*
BASIC GAME RULES:

- The computer generates a random number between 1 and 1000
- The player has 20 tries to guess it
- On each try the player gets a hint if they guessed too high or too low
- All hints are stored in a log for future reference 
- The player wins if they guess the number before they run out of tries
- Otherwise, the player loses the game
*/

//FUNCTIONs below this line:

//FUNCTION to generate random number between 1 and 20
const randomNumber = () => Math.trunc(Math.random() * 1000) + 1;

//Variables below this line:

//All class selectors go here
const selectBody = document.querySelector('body');
const selectMessage = document.querySelector('.message');
const selectNumber = document.querySelector('.number');
const selectGuess = document.querySelector('.guess');
const selectCheck = document.querySelector('.check');
const selectScore = document.querySelector('.score');
const selectHighscore = document.querySelector('.highscore');
const selectAgain = document.querySelector('.again');
const selectLog = document.querySelector('.log');
const selectToggleLog = document.querySelector('.toggle-log');

// All other variables go here
let secretNumber = randomNumber();
let score = 20;
let highscore = 0;
let logs = [];

// EVENT LISTENER:
// On check button click
document.querySelector('.check').addEventListener('click', function () {
  const guess = +selectGuess.value;
  selectGuess.value = '';
  //FIXME development only
  console.log(guess, typeof guess);

  //CASE: No valid input value
  if (!guess || guess > 1000 || guess < 0) {
    selectMessage.textContent =
      'Please enter a number between 1 and 1000 to play.';

    //Guess is right & Player wins
  } else if (guess === secretNumber) {
    //Set secret number to user number and show it for reference &
    //Change info text to let user know they won
    selectNumber.textContent = guess;
    selectMessage.textContent = `🥳 You guessed the number!`;

    //Disable guess input and Check button
    selectGuess.disabled = true;
    selectCheck.disabled = true;

    //Change backgrounds
    selectBody.style.backgroundColor = '#60b347';
    selectNumber.style.width = '30rem';

    //Hide log
    selectToggleLog.style.display = 'none';

    //Update highscore if needed
    if (score > highscore) {
      highscore = score;
      selectHighscore.textContent = highscore;
    }

    //CASE: Guess is wrong
  } else if (guess !== secretNumber) {
    if (score > 1) {
      selectToggleLog.style.display = 'block';
      //Decrease score if guess is wrong &
      //Update info message according to case
      score--;
      selectScore.textContent = score;
      selectMessage.textContent =
        guess > secretNumber
          ? `😋 You guessed too high!`
          : `😋 You guessed too low!`;

      //Add current guess to logs[] &
      //Update log list
      logs = [...logs, guess];
      selectLog.innerHTML = logs
        .map(
          (log, index) =>
            `<li>Guess ${index + 1}: ${log} was ${
              log > secretNumber ? `too high!` : `too low!`
            }</li>`
        )
        .join('');

      //FIXME development only
      console.log(score);

      //CASE: Game over
    } else {
      //Show the secret number for reference &
      //Update info message to let user know the game is over
      selectNumber.textContent = secretNumber;
      selectMessage.textContent = '💣 You lost! Better luck next time!';

      //Disable guess input and Check button
      selectGuess.disabled = true;
      selectCheck.disabled = true;

      //Set score to 0
      selectScore.textContent = 0;

      //Change backgrounds
      selectBody.style.backgroundColor = '#cc4747';
      selectNumber.style.width = '30rem';
    }
  }
});

// EVENT LISTENER:
// On again button click
document.querySelector('.again').addEventListener('click', function () {
  //Reset score, secret number and logs[]
  score = 20;
  secretNumber = randomNumber();
  logs = [];

  //Hide secret number, log list &
  //Reset info message, guess input, score field
  selectNumber.textContent = '?';
  selectMessage.textContent = 'Start guessing...';
  selectGuess.value = '';
  selectScore.textContent = score;
  selectToggleLog.style.display = 'none';

  //Enable guess input and Check button
  selectGuess.disabled = false;
  selectCheck.disabled = false;

  //Reset backgrounds
  selectBody.style.backgroundColor = '#222';
  selectNumber.style.width = '15rem';
});

// EVENT LISTENER:
//Select guess input on focus
selectGuess.addEventListener('click', function () {
  this.select();
});
