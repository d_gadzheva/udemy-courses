'use strict';

// Data
const account1 = {
  owner: 'Jonas Schmedtmann',
  movements: [200, 450, -400, 3000, -650, -130, 70, 1300],
  interestRate: 1.2, // %
  pin: 1111,
  movementsDates: [
    '2020-11-18T21:31:17.178Z',
    '2020-12-23T07:42:02.383Z',
    '2020-12-28T09:15:04.904Z',
    '2021-01-01T10:17:24.185Z',
    '2021-02-08T14:11:59.604Z',
    '2021-03-20T17:01:17.194Z',
    '2021-03-22T23:36:17.929Z',
    '2021-03-23T10:51:36.790Z',
  ],
  currency: 'EUR',
  locale: 'pt-PT',
};

const account2 = {
  owner: 'Jessica Davis',
  movements: [5000, 3400, -150, -790, -3210, -1000, 8500, -30],
  interestRate: 1.5,
  pin: 2222,
  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-US',
};

const account3 = {
  owner: 'Steven Thomas Williams',
  movements: [200, -200, 340, -300, -20, 50, 400, -460],
  interestRate: 0.7,
  pin: 3333,
  movementsDates: [
    '2019-11-18T21:31:17.178Z',
    '2019-12-23T07:42:02.383Z',
    '2020-01-28T09:15:04.904Z',
    '2020-04-01T10:17:24.185Z',
    '2020-05-08T14:11:59.604Z',
    '2020-05-27T17:01:17.194Z',
    '2020-07-11T23:36:17.929Z',
    '2020-07-12T10:51:36.790Z',
  ],
  currency: 'LEV',
  locale: 'bg-BG',
};

const account4 = {
  owner: 'Sarah Smith',
  movements: [430, 1000, 700, 50, 90],
  interestRate: 1,
  pin: 4444,
  movementsDates: [
    '2019-11-01T13:15:33.035Z',
    '2019-11-30T09:48:16.867Z',
    '2019-12-25T06:04:23.907Z',
    '2020-01-25T14:18:46.235Z',
    '2020-02-05T16:33:06.386Z',
    '2020-04-10T14:43:26.374Z',
    '2020-06-25T18:49:59.371Z',
    '2020-07-26T12:01:20.894Z',
  ],
  currency: 'USD',
  locale: 'en-GB',
};

const accounts = [account1, account2, account3, account4];

// Elements
const labelWelcome = document.querySelector('.welcome');
const labelDate = document.querySelector('.date');
const labelBalance = document.querySelector('.balance__value');
const labelSumIn = document.querySelector('.summary__value--in');
const labelSumOut = document.querySelector('.summary__value--out');
const labelSumInterest = document.querySelector('.summary__value--interest');
const labelTimer = document.querySelector('.timer');

const containerApp = document.querySelector('.app');
const containerMovements = document.querySelector('.movements');

const btnLogin = document.querySelector('.login__btn');
const btnTransfer = document.querySelector('.form__btn--transfer');
const btnLoan = document.querySelector('.form__btn--loan');
const btnClose = document.querySelector('.form__btn--close');
const btnSort = document.querySelector('.btn--sort');

const inputLoginUsername = document.querySelector('.login__input--user');
const inputLoginPin = document.querySelector('.login__input--pin');
const inputTransferTo = document.querySelector('.form__input--to');
const inputTransferAmount = document.querySelector('.form__input--amount');
const inputLoanAmount = document.querySelector('.form__input--loan-amount');
const inputCloseUsername = document.querySelector('.form__input--user');
const inputClosePin = document.querySelector('.form__input--pin');

// FUNCTION:
// Function to generate usernames for all users based on their initials
const generateUsernames = (accs) => {
  accs.forEach((acc) => {
    acc.username = acc.owner
      .toLowerCase()
      .split(' ')
      .map((name) => name[0])
      .join('');
  });
};
generateUsernames(accounts);

// FUNCTION:
// Function to format movement's date
const formatMovementDate = (date, locale) => {
  // FUNCTION
  // Function to calculate days passed
  const calcDaysPassed = (date1, date2) =>
    Math.round(Math.abs(date1 - date2) / (1000 * 60 * 60 * 24));
  // Format dates based on days passed
  const daysPassed = calcDaysPassed(new Date(), date);
  if (daysPassed === 0) return 'Today';
  if (daysPassed === 1) return 'Yesterday';
  if (daysPassed <= 7) return `${daysPassed} days ago`;

  return new Intl.DateTimeFormat(locale).format(date);
};

// FUNCTION:
// Function to format currency according to locale
const formatCurr = (value, locale, currency) => {
  return new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currency,
  }).format(value);
};

// FUNCTION:
// Function to display all the movements of the account
// Deposits and Withdrawals
const displayMovements = function (acc, sort = false) {
  containerMovements.innerHTML = '';

  // Sort movements array if function called with sort = true
  const movs = sort
    ? acc.movements.slice().sort((a, b) => a - b)
    : acc.movements;

  movs.forEach((mov, i) => {
    const type = mov > 0 ? 'deposit' : `withdrawal`;
    const date = new Date(acc.movementsDates[i]);
    const displayDate = formatMovementDate(date, acc.locale);

    const formattedMov = formatCurr(mov, acc.locale, acc.currency);

    const html = `
      <div class="movements__row">
        <div class="movements__type movements__type--${type}">
        ${i + 1} ${type}</div>
        <div class="movements__date">${displayDate}</div>
        <div class="movements__value">${formattedMov}</div>
      </div>`;

    containerMovements.insertAdjacentHTML('afterbegin', html);
  });
};

// FUNCTION:
// Function to calculate the current balance of the account
const calcDisplayBalance = (acc) => {
  acc.balance = acc.movements.reduce((acc, mov) => acc + mov, 0);
  labelBalance.innerHTML = `${formatCurr(
    acc.balance,
    acc.locale,
    acc.currency
  )}`;
};

// FUNCTION:
// Function to calculate the summary of the account
// Ins vs Outs vs Interest
const calcDisplaySummary = (acc) => {
  const incomes = acc.movements
    .filter((mov) => mov > 0)
    .reduce((sum, deposit) => sum + deposit, 0);
  labelSumIn.innerHTML = `${formatCurr(incomes, acc.locale, acc.currency)}`;

  const outcomes = acc.movements
    .filter((mov) => mov < 0)
    .reduce((sum, withdrawal) => sum + withdrawal, 0);
  labelSumOut.innerHTML = `${formatCurr(
    Math.abs(outcomes),
    acc.locale,
    acc.currency
  )}`;

  const interest = acc.movements
    .filter((mov) => mov > 0)
    .map((deposit) => (deposit * acc.interestRate) / 100)
    .filter((int) => int >= 1)
    .reduce((sum, int) => sum + int, 0);
  labelSumInterest.innerHTML = `${formatCurr(
    interest,
    acc.locale,
    acc.currency
  )}`;
};

// FUNCTION:
// Function to update UI based on given account
const updateUI = (acc) => {
  // Calculate movements for the account
  displayMovements(acc);
  // Calculate balance for the account
  calcDisplayBalance(acc);
  // Display summary for the account
  calcDisplaySummary(acc);
};

// FUNCTION:
// Function to store timer
const startLogoutTimer = () => {
  // FUNCTION:
  // setInterval callback ()
  const tick = () => {
    // Print timer to UI
    const min = String(Math.trunc(time / 60)).padStart(2, 0);
    const sec = String(Math.trunc(time % 60)).padStart(2, 0);
    labelTimer.textContent = `${min}:${sec}`;

    // Stop timer and logout user when timer = 0
    if (time === 0) {
      clearInterval(timer);
      labelWelcome.textContent = `Login to get started`;
      containerApp.style.opacity = 0;
    }

    // Update timer;
    time--;
  };
  // Set timer to 5 mins
  let time = 300;

  // Update timer every sec
  tick();
  const timer = setInterval(tick, 1000);

  // Return timer to keep track if timer exists outside the ()
  return timer;
};

let currentUser, timer;

// EVENT LISTENER:
// On login
btnLogin.addEventListener('click', (e) => {
  e.preventDefault();

  // Get Username from login input
  currentUser = accounts.find(
    (acc) => acc.username === inputLoginUsername.value
  );

  // CASE: If username exists && pin correct -> login
  if (currentUser?.pin === +inputLoginPin.value) {
    // Clear login data
    inputLoginUsername.value = inputLoginPin.value = '';
    inputLoginPin.blur();
    // Start logout timer
    if (timer) clearInterval(timer);
    timer = startLogoutTimer();
    // Upade welcome message
    labelWelcome.textContent = `Welcome back, ${
      currentUser.owner.split(' ')[0]
    }`;
    // Get current date &&
    // way 1
    // Set it to UI
    // const now = new Date();
    // const year = now.getFullYear();
    // const month = `${now.getMonth() + 1}`.padStart(2, 0);
    // const day = `${now.getDate()}`.padStart(2, 0);
    // const hour = `${now.getHours()}`.padStart(2, 0);
    // const min = `${now.getMinutes()}`.padStart(2, 0);
    // labelDate.textContent = `${day}/${month}/${year}, ${hour}:${min}`;
    // way 2
    const now = new Date();
    const options = {
      minute: 'numeric',
      hour: 'numeric',
      day: 'numeric',
      month: 'long',
      year: 'numeric',
      weekday: 'short',
    };
    // Get browser language
    const locale = navigator.language;
    labelDate.textContent = new Intl.DateTimeFormat(locale, options).format(
      now
    );
    // Show UI
    containerApp.style.opacity = 100;
    updateUI(currentUser);

    // CASE: invalid credentials
  } else {
    // Hide UI
    containerApp.style.opacity = 0;
    // Update welcome message
    labelWelcome.textContent = 'Invalid credentials';
  }
});

// EVENT LISTENER:
// On money transfer
btnTransfer.addEventListener('click', (e) => {
  e.preventDefault();
  // Get amount to transfer
  const amount = +inputTransferAmount.value;

  // Get acount to transfer to
  const recieverAcc = accounts.find(
    (acc) => acc.username === inputTransferTo.value
  );

  // Clear transfer inputs
  inputTransferTo.value = inputTransferAmount.value = '';
  inputTransferAmount.blur();

  // Check if amount is > 0 &&
  // User have enough money to transfer &&
  // recieverAcc exists &&
  // recieverAcc is different than the currentUser
  // CASE: transfer valid
  if (
    amount > 0 &&
    amount <= currentUser.balance &&
    recieverAcc &&
    recieverAcc.username !== currentUser.username
  ) {
    // If account exist:
    // Transfer amount &&
    // Withdraw amount from current User
    // Set dates for both movements
    // Update UI
    recieverAcc.movements.push(amount);
    currentUser.movements.push(amount * -1);
    recieverAcc.movementsDates.push(new Date().toISOString());
    currentUser.movementsDates.push(new Date().toISOString());
    updateUI(currentUser);
    //Reset logout timer
    clearInterval(timer);
    timer = startLogoutTimer();
  }
});

// EVENT LISTENER:
// On load request
btnLoan.addEventListener('click', (e) => {
  e.preventDefault();
  // Get amount from input
  const amount = Math.floor(inputLoanAmount.value);

  // If amount > 0 && user has a deposit >= 10% of the requested loan
  // CASE: loan approved
  if (amount > 0 && currentUser.movements.some((mov) => mov >= amount * 0.1)) {
    // Give loan
    currentUser.movements.push(amount);
    // Set date for the loan
    currentUser.movementsDates.push(new Date().toISOString());
    // UpdateUI
    updateUI(currentUser);
    //Reset logout timer
    clearInterval(timer);
    timer = startLogoutTimer();
  }
  // Clear input
  inputLoanAmount.value = '';
  //TODO: visual feedback on what's going on
});

// EVENT LISTENER:
// On movements sort

// Variable to keep track if movements already sorted
let sorted = false;
btnSort.addEventListener('click', (e) => {
  e.preventDefault();
  // sort the movements if not sorted OR
  // go back to chronical order if sorted
  displayMovements(currentUser, !sorted);
  // switch sorted variable
  sorted = !sorted;
});

// EVENT LISTENER:
// On account delete
btnClose.addEventListener('click', (e) => {
  e.preventDefault();

  // Check if input username and password match the current user username and password
  // CASE: credentials match
  if (
    inputCloseUsername.value === currentUser.username &&
    +inputClosePin.value === currentUser.pin
  ) {
    // Clear input data
    inputCloseUsername.value = inputClosePin.value = '';
    //const index = accounts.map(acc => acc).indexOf(currentUser);
    // Get index of account
    const index = accounts.findIndex(
      (acc) => acc.username === currentUser.username
    );
    // Delete it
    accounts.splice(index, 1);
    // Hide UI
    containerApp.style.opacity = 0;
  }
});
