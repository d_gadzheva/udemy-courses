## 1. Omnifood

**Purpose**

The idea behind this project was to teach the basics of HTML5 and CSS3 to everyone who may be insterested.

I did it for fun.

**Tools used**

Written on VS Code using plain HTML5, CSS3 and a tiny bit of jQuery for chic.

[Live demo Omnifood](https://vigilant-fermat-784da2.netlify.com/)

---

## 2, 3, 4. Natours, Trillo and Nexter

**Purpose**

All three projects were a part of the Advanced CSS and SASS course.
Natours laid the foundations, introducing tons of modern CSS techniques, advanced CSS animations, advanced responsive design and responsive images.  
What is SASS, how to use it and how to best structure your files. Also gave a nice introduction to the BEM methodology.
Added just a speck of jQuery for the navigation so the page doesn’t feel static.

Trillo's main goal was to introduce CSS Flexbox. That's exactly why everything that could've been done using Flexbox is done with Flexbox.
Jonas (the lecturer) decided not to structure his SASS code during this project so I tried to do this on my own.
The image hover, user navigation menu, chat details and the search results are 100% my deed. Jonas left us to have some fun.

Nexter's main goal was the CSS Grid Layout. Again, everything that could've been done with grid, is done with grid.

**Tools used**

Written on VS Code using plain HTML5, SASS (the SCSS syntax) and the BEM methodology.

[Live demo Natours](https://heuristic-pike-ff578d.netlify.com/) |
[Live demo Trillo](https://friendly-lamarr-51bcc4.netlify.com/) |
[Live demo Nexter](https://vibrant-payne-217a3f.netlify.com/)

---

## 5. Pig Game

**Purpose**

GAME RULES:

- The game has 2 players, playing in rounds.
- In each turn, a player rolls a dice as many times as he wishes. Each result get added to his ROUND score.
- BUT
  - if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn.
  - if the player rolls a 6 in a roll, all his GLOBAL score gets lost. After that, it's the next player's turn.
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score. After that, it's the next player's turn.
- The first player to reach 100 points (by default) on GLOBAL score wins the game. Players can change the winning score.

**Tools used**

Written on VS Code using ES5. (The HTML and CSS were provided by the lecturer.)

[Live demo Pig Game](https://nifty-leavitt-92e05b.netlify.com/)

---

## 6. Forkify v2021

**Purpose**

Forkify is a website, where you can search through cooking recipes, provided by an [external API](https://forkify-api.herokuapp.com/).
You can search for 'pasta' or 'pizza' or 'cake' etc and you will get a list of recipes to browse through. You can bookmark your favorites and add your own recipes as well. The recipes you add are only visible to you.

**Notes**

Refactored version of the previous Forkify due to the shutting down of the original API used in the course. If you download the code => npm i => npm start (:

**Tools used**

Written on VS Code using ES Modules, the MVC architecture and Parcel. (The HTML and CSS were provided by the lecturer.)

[Live demo Forkify](https://watery-books.surge.sh/)

---

## 7. Guess My Number

**Purpose**

GAME RULES:

- The computer generates a random number between 1 and 1000.
- The player has 20 attempts to guess it.
- On each attempt the player gets a hint if they guessed too high or too low.
- All hints are stored in a log for future reference.
- The player wins if they guess the number before they run out of attempts.
- Otherwise, the player loses the game.

**Notes**

In the original version of the game, there were some problems presented:

- The player could play endlessly, regardless of the current game status;
- There was no visual highlight on any status change - the player WON or LOST the game;
- The player could enter ANY number as their guess (less than 0 or greater than 1000);
- There was no visual feedback addressing this issue, which resulted in wasted attempts and confusion (yes, invalid inputs were still counted as attempts).

So I also had to add some validations around the guess number value and some logic for the WIN or LOSE scenarios. Also, the log list containing all the guesses was not original to the course content. I added it because I found keeping track of what is going on in the game rather challenging.

**Tools used**

Written on VS Code using ES6+. (The HTML and CSS were provided by the lecturer.)

[Live demo Guess My Number](https://rainy-sound.surge.sh/)

---

## 8. Pig Game v2021

**Purpose**

GAME RULES:

- The game has 2 players, playing in rounds.
- In each turn, a player rolls a dice as many times as he wishes. Each result get added to his ROUND score.
- BUT if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn.
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score. After that, it's the next player's turn.
- The first player to reach 100 points (by default) on GLOBAL score wins the game. Players can change the winning score.

**Notes**

It is the same Pig Game but this time built using ES6+. I played a little with the look to give it a more modern feeling. I took a new approach around the logic behind switching players when one of them rolls 1. Also, I gave the players the ability to customize their names and added a modal with the game rules because why not?
(Probably a good spot to mention that the customizable final score to win the game was not part of the course content and was added by me in both versions of the game.)

**Tools used**

Written on VS Code using ES6+. (The HTML and CSS were provided by the lecturer and edited by me to accomplish a more modern look.)

[Live demo Pig Game 2021](https://omniscient-popcorn.surge.sh/)

---

## 9. Bankist App

**Purpose**

Simulates a bank account. Once logged in, the user can see their account movements, transfer money to other users or request loans from the bank. The main goal of the project was to introduce working with objects and get the concept of Intl.

**Notes**

You can log in by using one of the following:

- user: js and pin: 1111
- user: jd and pin: 2222
- user: stw and pin: 3333
- user: ss and pin: 4444

Transfers can be made to:

- js OR jd OR stw OR ss

You cannot request loans bigger than 10 times your biggest positive movement (currently, there is no visual feedback on invalid loan requests).

If you have been inactive for 5 minutes, you will be logged out automatically.

**Tools used**

Written on VS Code using ES6+. (The HTML and CSS were provided by the lecturer.)

[Live demo Bankist App](https://hoc-crack.surge.sh/)

---

## 10. Bankist Website

**Purpose**

The official website for the Bankist app. The main goal of the project was to get into the advanced DOM manipulations such as animations, navigation, tabs, sliders and lazy loading of images.

**Notes**

The project did not have much potential for adding custom functionality, so I made it responsive (apart from the mobile nav). The code is not optimized as I did not want to spend too much time on it and decided to leave it for future exercise and continue with the course content.

**Tools used**

Written on VS Code using ES6+. (The HTML and CSS were provided by the lecturer. The media queries are written by me.)

[Live demo Banist Website](https://pleasant-smell.surge.sh/)

---

## 11. Mapty

**Purpose**

All your workouts in one place! Once you permit Mapty to get your location, you can start adding workouts by simply clicking on the map. A form will appear in the sidebar where you can add all relevant information for the workout. On form submit, your workout will appear on the map and a list in the sidebar. You can go to a specific workout from your list by clicking on it. You can delete a workout by clicking on x in the workouts list. (All workouts are stored in localhost for future use.)

**Notes**

The original project had no validations whatsoever. All validations, both as logic and visual feedback, are done by me. The delete functionality is done by me as well, together with the custom pins on the map.

**Tools used**

Written on VS Code using ES Classes. Build with the help of [Leaflet](https://leafletjs.com/) (The HTML and CSS were provided by the lecturer. All notifications are made by me.)

[Live demo Mapty](https://poppy-field.surge.sh/)

---

## 12. React Playgrounds

**Purpose**

Compilation of mini-apps which sole purpose is to get started with React.

**Notes**

More information in the folder. If you download any of the code => npm i => npm start (:

---
